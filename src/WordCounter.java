/**
 * Created by Dmitry on 7/15/2015.
 */
import java.util.Scanner;

public class WordCounter {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String input;
        System.out.print("Enter a string: ");
        input = keyboard.nextLine();

        if (input.isEmpty()) {
            System.out.print("-----------------------");
            System.out.print("\nNo words to count.");
        } else {
            int count = check(input);
            if (count == 1) {
                    System.out.print("------------------------------------");
                    System.out.print("\nThere is only  " + count + " word in the string.");
                } else {
                    System.out.print("------------------------------------");
                    System.out.print("\nThere are  " + count + " words in the string.");
                }
            }
        keyboard.close();
    }

    private static int check(String str) {
        String[] result = str.split(" ");
        return result.length;
    }
}
